import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function App() {
	const [darkMode, setDarkMode] = React.useState(getInitialMode())
	React.useEffect(() => {
		localStorage.setItem('dark', JSON.stringify(darkMode))
	}, [darkMode])

	function getInitialMode() {
		const isReturningUser = 'dark' in localStorage
		const saveMode = JSON.parse(localStorage.getItem('dark'))
		const userPreferDark = getPrefColorScheme()

		if (isReturningUser) {
			return saveMode
		} else if (userPreferDark) {
			return true
		} else {
			return false
		}
	}

	function getPrefColorScheme() {
		if (!window.matchMedia) return

		return window.matchMedia('(prefers-color-scheme: dark)').matches
	}

	return (
		<div className={darkMode ? "dark-mode" : "light-mode"}>
			<nav>
				<div className="toggle-container">
					<span style={{color: darkMode ? "grey" : "yellow"}}>☀︎</span>
					<span className="toggle">
						<input
							checked={darkMode}
							onChange={() => setDarkMode(prevMode => !prevMode)}
							type="checkbox"
							className="checkbox"
							id="checkbox"
						/>
						<label htmlFor="checkbox" />
					</span>
					<span style={{color: darkMode ? "slateblue" : "grey"}}>☾</span>
				</div>
			</nav>
			<main>
				<h1>{darkMode ? "Dark Mode" : "Light Mode"}</h1>
				<h2>Toggle the switch to see some magic happen!</h2>
			</main>
		</div>
		)
}

ReactDOM.render(<App />, document.getElementById('root'));
